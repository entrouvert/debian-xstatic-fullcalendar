from xstatic.pkg import fullcalendar as fc

# The README.txt file should be written in reST so that PyPI can use
# it to generate your project's PyPI page.
long_description = open('README.txt').read()

from setuptools import setup, find_packages

setup(
    name=fc.PACKAGE_NAME,
    version=fc.PACKAGE_VERSION,
    description=fc.DESCRIPTION,
    long_description=long_description,
    classifiers=fc.CLASSIFIERS,
    keywords=fc.KEYWORDS,
    maintainer=fc.MAINTAINER,
    maintainer_email=fc.MAINTAINER_EMAIL,
    license=fc.LICENSE,
    url=fc.HOMEPAGE,
    platforms=fc.PLATFORMS,
    packages=find_packages(),
    namespace_packages=['xstatic', 'xstatic.pkg', ],
    include_package_data=True,
    zip_safe=False,
    install_requires=[],  # nothing! :)
                          # if you like, you MAY use the 'XStatic' package.
)
